import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> filter = <String>["All", "2", "3", "3", "3"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: 0,
        selectedItemColor: Colors.deepPurpleAccent,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.wrap_text),
            title: new Text("home"),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.calendar_today),
            title: new Text("Calender"),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.favorite_border),
            title: new Text("favorite"),
          )
        ],
      ),
      appBar: MyCustomAppBar(
        height: 80,
        topHeight: MediaQuery.of(context).padding.top,
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Divider(
                thickness: 1,
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 14),
                child: Text(
                  "Popular",
                  style: TextStyle(
                      color: Colors.deepPurpleAccent,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
              Container(
                height: 240,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Container(
                      width: 292,
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 15, bottom: 20),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              color:
                                  Theme.of(context).focusColor.withOpacity(0.1),
                              blurRadius: 15,
                              offset: Offset(0, 5)),
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Stack(
                            fit: StackFit.loose,
                            alignment: AlignmentDirectional.centerStart,
                            children: <Widget>[
                              ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: Image.asset(
                                    'assets/img/f2.jpeg',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 150,
                                  )),
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Icon(
                                    Icons.arrow_back_ios_outlined,
                                    color: Colors.white,
                                  )),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Icon(
                                    Icons.arrow_forward_ios_rounded,
                                    color: Colors.white,
                                  ))
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 20, right: 20),
                            child: Expanded(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Text(
                                    "Fast and Furious",
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: Theme.of(context).textTheme.subhead,
                                  ),
                                  Icon(
                                    Icons.favorite_border,
                                    color: Colors.deepPurpleAccent,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
              Divider(
                thickness: 1,
              ),
              Container(
                height: 60,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Container(
                      height: 30,
                      width: 80,
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 15, bottom: 20),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                              color:
                                  Theme.of(context).focusColor.withOpacity(0.1),
                              blurRadius: 15,
                              offset: Offset(0, 5)),
                        ],
                      ),
                      child: Center(
                          child: Text(
                        filter[index].toString(),
                        style: TextStyle(fontSize: 10),
                      )),
                    );
                  },
                ),
              ),
              Container(
                height: 70,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Container(
                      height: 30,
                      width: 80,
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 15, bottom: 20),
                      decoration: BoxDecoration(
                        border: Border.all(
                            width: 1, color: Colors.deepPurpleAccent),
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                              color:
                                  Theme.of(context).focusColor.withOpacity(0.1),
                              blurRadius: 15,
                              offset: Offset(0, 5)),
                        ],
                      ),
                      child: Center(child: Text(filter[index].toString())),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 14),
                child: Text(
                  "Today",
                  style: TextStyle(
                      color: Colors.deepPurpleAccent,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
              Container(
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  primary: true,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Container(
                      width: 292,
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 15, bottom: 20),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              color:
                                  Theme.of(context).focusColor.withOpacity(0.1),
                              blurRadius: 15,
                              offset: Offset(0, 5)),
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Stack(
                            fit: StackFit.loose,
                            alignment: AlignmentDirectional.centerStart,
                            children: <Widget>[
                              ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: Image.asset(
                                    'assets/img/f2.jpeg',
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                    height: 150,
                                  )),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 20, right: 20),
                            child: Expanded(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Text(
                                    "Fast and Furious",
                                    overflow: TextOverflow.fade,
                                    softWrap: false,
                                    style: Theme.of(context).textTheme.subhead,
                                  ),
                                  Icon(
                                    Icons.favorite_border,
                                    color: Colors.deepPurpleAccent,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.start),
      ),
    );
  }
}

class MyCustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final double topHeight;

  const MyCustomAppBar(
      {Key key, this.parentScaffoldKey, @required this.height, this.topHeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(highlightColor: Colors.transparent),
      child: Container(
        height: height,
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: topHeight),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 10),
              child: Container(
                decoration: new BoxDecoration(
                    border:
                        Border.all(width: 1, color: Colors.deepPurpleAccent),
                    borderRadius: new BorderRadius.all(Radius.circular(35.0))),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                    child: Container(
                      height: 35,
                      child: Image.asset("assets/img/prof.jpg"),
                    )),
              ),
            ),
            Expanded(
                child: Text(
              "Messi",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            )),
            Icon(Icons.search, color: Colors.deepPurpleAccent),
            SizedBox(
              width: 5,
            ),
            Icon(
              Icons.notifications_none,
              color: Colors.deepPurpleAccent,
            ),
            SizedBox(
              width: 14,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
